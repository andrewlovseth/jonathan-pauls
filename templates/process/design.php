<?php

    $design = get_field('design');
    $headline = $design['headline'];
    $copy = $design['copy'];

    if(have_rows('design')): while(have_rows('design')): the_row();

?>

    <section class="design grid">

        <div class="phase-header">
            <div class="headline">
                <h3 class="section-header"><?php echo $headline; ?></h3>
            </div>

            <div class="copy-1">
                <?php echo $copy; ?>
            </div>    
        </div>

        <?php if(have_rows('features')): ?>
            <div class="features">
                <?php while(have_rows('features')): the_row(); ?>

                    <div class="feature">
                        <?php $image = get_sub_field('photo'); if( $image ): ?>
                            <div class="photo">
                                <div class="content">
                                    <?php echo wp_get_attachment_image($image['ID'], 'large'); ?>
                                </div>
                            </div>
                        <?php endif; ?>

                        <div class="info">
                            <div class="headline">
                                <h4><?php echo get_sub_field('headline'); ?></h4>
                            </div>

                            <div class="copy-2 extended">
                                <?php echo get_sub_field('copy'); ?>                      
                            </div>
                        </div>                    
                    </div>

                <?php endwhile; ?>
            </div>
        <?php endif; ?>
        
    </section>

<?php endwhile; endif; ?>