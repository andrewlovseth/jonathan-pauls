<?php

    $overview = get_field('overview');
    $headline = $overview['headline'];

    if($headline): 

?>

    <section class="overview grid">
        <div class="copy-1">
            <p><?php echo $headline; ?></p>
        </div>
    </section>

<?php endif; ?>