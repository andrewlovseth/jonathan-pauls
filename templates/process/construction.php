<?php

    $construction = get_field('construction');
    $headline = $construction['headline'];
    $copy = $construction['copy'];

    if(have_rows('construction')): while(have_rows('construction')): the_row();

?>

    <section class="construction">

        <div class="phase-header grid">
            <div class="headline">
                <h3 class="section-header"><?php echo $headline; ?></h3>
            </div>

            <?php if($copy): ?>
                <div class="copy-1">
                    <?php echo $copy; ?>
                </div>   
            <?php endif; ?> 
        </div>

        <?php if(have_rows('features')): ?>
            <div class="features construction-slider">
                <?php while(have_rows('features')): the_row(); ?>

                    <div class="feature">
                        <?php $gallery = get_sub_field('gallery'); if( $gallery ): ?>
                            <div class="gallery">
                                <?php $i = 1; foreach( $gallery as $photo ): ?>
                                    <div class="photo photo-<?php echo $i; ?>">
                                        <div class="content">
                                            <?php echo wp_get_attachment_image($photo['ID'], 'large'); ?>
                                        </div>
                                    </div>                                
                                <?php $i++; endforeach; ?>
                            </div>
                        <?php else: ?>
                            <?php $image = get_sub_field('photo'); if( $image ): ?>
                                <div class="photo">
                                    <div class="content">
                                        <?php echo wp_get_attachment_image($image['ID'], 'large'); ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>

                        <div class="info">
                            <div class="headline">
                                <h4><?php echo get_sub_field('headline'); ?></h4>
                            </div>

                            <div class="copy-2">
                                <?php echo get_sub_field('copy'); ?>                      
                            </div>
                        </div>                    
                    </div>

                <?php endwhile; ?>
            </div>
        <?php endif; ?>
        
    </section>

<?php endwhile; endif; ?>