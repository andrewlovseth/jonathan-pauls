<?php

/*
  
    Template Name: About

*/

get_header(); ?>

    <?php get_template_part('template-parts/global/hero'); ?>

    <?php get_template_part('templates/about/overview'); ?>

    <?php get_template_part('templates/about/faqs'); ?>

    <?php get_template_part('templates/about/history'); ?>

<?php get_footer(); ?>