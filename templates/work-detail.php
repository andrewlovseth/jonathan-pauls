<?php

/*
  
    Template Name: Work Detail

*/

get_header(); ?>

    <?php get_template_part('template-parts/global/hero'); ?>

    <?php get_template_part('templates/work-detail/gallery'); ?>

    <?php get_template_part('templates/work-detail/case-study'); ?>

    <?php get_template_part('template-parts/footer/about-contact'); ?>

<?php get_footer(); ?>