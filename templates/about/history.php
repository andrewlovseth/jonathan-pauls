<?php

    $history = get_field('history');
    $headline = $history['headline'];
    $copy = $history['copy'];

    if(have_rows('history')): while(have_rows('history')): the_row();

?>

    <section class="history">

        <div class="history-header grid">
            <div class="headline">
                <h3 class="section-header"><?php echo $headline; ?></h3>
            </div>

            <div class="copy-1">
                <?php echo $copy; ?>
            </div>    
        </div>

        <?php if(have_rows('timeline')): ?>
            <div class="timeline">
                <div class="timeline-slider">
                    <?php while(have_rows('timeline')): the_row(); ?>

                        <div class="event">
                            <?php $photo = get_sub_field('photo'); if( $photo ): ?>
                                <div class="photo">
                                    <div class="content">
                                        <?php echo wp_get_attachment_image($photo['ID'], 'large'); ?>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <div class="info">
                                <div class="year">
                                    <h4><?php echo get_sub_field('year'); ?></h4>
                                </div>

                                <div class="caption copy-2">
                                    <p><?php echo get_sub_field('caption'); ?></p>                        
                                </div>
                            </div>                    
                        </div>

                    <?php endwhile; ?>
                </div>
            </div>
        <?php endif; ?>
        
    </section>

<?php endwhile; endif; ?>