<?php

    $overview = get_field('overview');
    $headline = $overview['headline'];
    $copy = $overview['copy'];
    $photos = $overview['photos'];

?>

<section class="overview grid">
    <div class="info">
        <div class="headline">
            <h3 class="section-header"><?php echo $headline; ?></h3>
        </div>

        <div class="copy-1 extended">
            <?php echo $copy; ?>
        </div>    
    </div>

    <div class="gallery">   
        <?php $count = 1; foreach( $photos as $photo ): ?>
            <div class="photo photo-<?php echo $count; ?>">
                <div class="content">
                    <?php echo wp_get_attachment_image($photo['ID'], 'large'); ?>
                </div>
            </div>
        <?php $count++; endforeach; ?>
    </div>
</section>