<?php

    $headline = get_field('faqs_headline');
    $show = get_field('faqs_show');

    if($show):

?>

    <section class="faqs grid">
        <div class="headline">
            <h3 class="section-header"><?php echo $headline; ?></h3>
        </div>

        <?php if(have_rows('faqs')): ?>
            <div class="faq-list">
                <?php $count = 1; while(have_rows('faqs')): the_row(); ?>
                    
                    <div class="faq<?php if($count == 1): ?> active<?php endif; ?>">
                        <div class="question">
                            <h4><?php echo get_sub_field('question'); ?></h4>
                        </div>

                        <div class="answer copy-2 extended">
                            <?php echo get_sub_field('answer'); ?>
                        </div>                
                    </div>
                <?php $count++; endwhile; ?>
            </div>
        <?php endif; ?>
    </section>

<?php endif; ?>