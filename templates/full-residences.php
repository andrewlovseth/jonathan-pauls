<?php

/*
  
    Template Name: Full Residences

*/

get_header(); ?>

    <?php get_template_part('template-parts/global/hero'); ?>

    <?php get_template_part('templates/full-residences/residences'); ?>

    <?php get_template_part('template-parts/footer/about-contact'); ?>

<?php get_footer(); ?>