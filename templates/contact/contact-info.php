<?php

    $contact = get_field('contact');
    $headline = $contact['headline'];
    $copy = $contact['copy'];

    $map_link = $contact['map_link'];
    $map_image = $contact['map_image'];

?>

<section class="contact-info grid">
    <div class="contact-info-wrapper">
        <div class="headline">
            <h3 class="section-header"><?php echo $headline; ?></h3>
        </div>

        <div class="copy-1">
            <?php echo $copy; ?>
        </div>

        <div class="contact-details">
            <div class="col">
                <div class="address detail">
                    <div class="detail-header">
                        <h4>Address</h4>
                    </div>

                    <div class="copy-2">
                        <p><?php echo get_field('address', 'options'); ?></p>
                    </div>
                </div>

                <div class="phone detail">
                    <div class="detail-header">
                        <h4>Phone</h4>
                    </div>

                    <div class="copy-2">
                        <p><?php echo get_field('phone', 'options'); ?></p>
                    </div>
                </div>

                <div class="email detail">
                    <div class="detail-header">
                        <h4>Email</h4>
                    </div>

                    <div class="copy-2">
                        <p><a href="mailto:<?php echo get_field('email', 'options'); ?>"><?php echo get_field('email', 'options'); ?></a></p>
                    </div>
                </div>
            </div>

            <div class="col">
                <div class="hours detail">
                    <div class="detail-header">
                        <h4>Hours</h4>
                    </div>

                    <div class="copy-2">
                        <p><?php echo get_field('hours', 'options'); ?></p>
                    </div>
                </div>

                <div class="social detail">
                    <div class="detail-header">
                        <h4>Social</h4>
                    </div>

                    <ul class="social-links">
                        <?php get_template_part('template-parts/global/social-links'); ?>
                    </ul>
                </div>            
            </div>
        </div>
    </div>

    <div class="map">
        <a href="<?php echo $map_link; ?>" rel="external">
            <?php echo wp_get_attachment_image($map_image['ID'], 'full'); ?>
        </a>
    </div>
</section>