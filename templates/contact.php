<?php

/*
  
    Template Name: Contact

*/

get_header(); ?>

    <?php get_template_part('template-parts/global/hero'); ?>

    <?php get_template_part('templates/contact/contact-info'); ?>

<?php get_footer(); ?>