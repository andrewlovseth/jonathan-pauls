<?php $residences = get_field('residences'); if( $residences ): ?>
	<?php foreach( $residences as $residence ): ?>
		<section class="residence grid">

            <div class="photo">
                <div class="content">
                    <?php $image = get_field('hero_photo', $residence->ID); if( $image ): ?>
                        <a href="<?php echo get_permalink( $residence->ID ); ?>">
                            <?php echo wp_get_attachment_image($image['ID'], 'large'); ?>
                        </a>
                    <?php endif; ?>
                </div>
            </div>

            <div class="info">
                <div class="headline">
                    <h3><a href="<?php echo get_permalink( $residence->ID ); ?>"><?php echo get_the_title( $residence->ID ); ?></a></h3>
                </div>
            </div>
			
		</section>
	<?php endforeach; ?>
<?php endif; ?>