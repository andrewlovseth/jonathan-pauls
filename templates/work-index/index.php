<section class="index grid">

    <?php if(have_rows('index_grid')): while(have_rows('index_grid')) : the_row(); ?>

        <?php if( get_row_layout() == 'row' ): ?>

            <div class="row columns-<?php echo get_sub_field('columns'); ?>">
                <?php if(have_rows('work_types')): while(have_rows('work_types')): the_row(); ?>

                    <?php
                        $work_type = get_sub_field('page');
                        $photo = get_sub_field('photo');
                        if( $work_type ):
                    ?>
                        <div class="work-type">
                            <div class="photo">
                                <div class="content">
                                    <a href="<?php echo get_permalink($work_type->ID); ?>">
                                        <?php echo wp_get_attachment_image($photo['ID'], 'large'); ?>
                                    </a>
                                </div>
                            </div>

                            <div class="label">
                                <h3>
                                    <a href="<?php echo get_permalink($work_type->ID); ?>">
                                        <?php echo get_the_title($work_type->ID); ?>
                                    </a>
                                </h3>
                            </div>                        
                        </div>
                    <?php endif; ?>

                <?php endwhile; endif; ?>			
            </div>

        <?php endif; ?>

    <?php endwhile; endif; ?>

</section>