<?php

    $case_study = get_field('case_study');
    $headline = $case_study['headline'];
    $copy = $case_study['copy'];
    $photos = $case_study['photos'];
    $vitals = $case_study['vitals'];

?>

<section class="case-study grid">
    <div class="case-study__header">
        <?php if($headline): ?>
            <div class="headline">
                <h3 class="section-header"><?php echo $headline; ?></h3>
            </div>
        <?php endif; ?>

        <?php if($copy): ?>
            <div class="copy-2">
                <?php echo $copy; ?>
            </div>
        <?php endif; ?>
    </div>


    <div class="case-study__info">
        <?php if($photos): ?>
            <div class="photos">
                <div class="case-study-slider">
                    <?php foreach( $photos as $photo ): ?>
                        <div class="photo">
                            <div class="photo-wrapper">
                                <div class="content">
                                    <?php echo wp_get_attachment_image($photo['ID'], 'full'); ?>
                                </div>
                            </div>
                        </div>        
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>

        <?php if($vitals): ?>
            <div class="vitals copy-2">
                <?php echo $vitals; ?>
            </div>
        <?php endif; ?>
    </div>
</section>