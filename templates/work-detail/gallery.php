<?php

    $headline = get_field('gallery_headline');
    $photos = get_field('gallery');

?>

<section class="gallery grid">
    <?php if($headline): ?>
        <div class="section-header">
            <div class="copy-1">
                <p><?php echo $headline; ?></p>
            </div>
        </div>
    <?php endif; ?>

    <?php if( $photos ): ?>
        
        <div class="photos-wrapper">
            <div class="photos">
                <?php foreach($photos as $photo): ?>
                    <div class="photo">
                        <a data-fslightbox href="<?php echo wp_get_attachment_image_url($photo['id'], 'full'); ?>">
                        <img src="<?php echo $photo['sizes']['medium']; ?>" alt="<?php echo $photo['alt']; ?>" />
                        </a>
                    </div>

                <?php endforeach; ?>
            </div>
        </div>

    <?php endif; ?>

</section>