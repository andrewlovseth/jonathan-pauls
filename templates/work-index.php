<?php

/*
  
    Template Name: Work Index

*/

get_header(); ?>

    <?php get_template_part('template-parts/global/hero'); ?>

    <?php get_template_part('templates/work-index/overview'); ?>

    <?php get_template_part('templates/work-index/index'); ?>

    <?php get_template_part('template-parts/footer/about-contact'); ?>

<?php get_footer(); ?>