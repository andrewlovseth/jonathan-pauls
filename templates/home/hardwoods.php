<?php

    $hardwoods = get_field('hardwoods');
    $headline = $hardwoods['headline'];
    $copy = $hardwoods['copy'];
    $link = $hardwoods['link'];
    $photo = $hardwoods['photo'];

?>

<section class="hardwoods grid">
    <div class="info">
        <div class="counter">
            <div class="counter-wrapper">
                <h5>01</h5>
            </div>
        </div>

        <div class="details">
            <div class="headline">
                <h2 class="section-header"><?php echo $headline; ?></h2>
            </div>

            <div class="copy-2">
                <?php echo $copy; ?>
            </div>

            <?php 
                if( $link ): 
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
            ?>

                <div class="cta">
                    <a class="underline" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                </div>

            <?php endif; ?>
        </div>
    </div>    
    
    <?php if( $photo ): ?>
        <div class="photo">
            <?php echo wp_get_attachment_image($photo['ID'], 'full'); ?>
        </div>
    <?php endif; ?>
</section>