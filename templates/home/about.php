<?php

    $about = get_field('about');
    $headline = $about['headline'];
    $sub_headline = $about['sub_headline'];
    $copy = $about['copy'];

?>

<section class="about grid">
    <div class="headline">
        <h2><?php echo $headline; ?></h2>
    </div>

    <?php if($sub_headline): ?>
        <div class="sub-headline">
            <h3><?php echo $sub_headline; ?></h3>
        </div>
    <?php endif; ?>

    <div class="copy-1">
        <?php echo $copy; ?>
    </div>
</section>