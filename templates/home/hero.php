<?php

    $hero = get_field('hero');
    $headline = $hero['headline'];
    $tagline = $hero['tagline'];
    $ctas = $hero['ctas'];

    if(have_rows('hero')): while(have_rows('hero')): the_row();

?>

    <section class="hero grid">
        <div class="hero-slider">
            <div class="hero-slider__wrapper">
                <?php if(have_rows('photos')): while(have_rows('photos')): the_row(); ?>
                    <div class="hero-slide">
                        <?php
                            $photo = get_sub_field('photo');
                            $page_link = get_sub_field('link');
                            $caption = get_sub_field('caption');
                        ?>
        
                        <div class="hero-content">
                            <div class="hero-content__wrapper">
                                <div class="headline">
                                    <h1><?php echo $headline; ?></h1>
                                </div>

                                <div class="tagline">
                                    <h2><?php echo $tagline; ?></h2>
                                </div>

                                <div class="ctas">
                                    <?php foreach($ctas as $cta): ?>
                    
                                        <?php 
                                            $link = $cta['link'];
                                            $style = $cta['style'];
                                            if( $link ): 
                                            $link_url = $link['url'];
                                            $link_title = $link['title'];
                                            $link_target = $link['target'] ? $link['target'] : '_self';
                                        ?>

                                            <div class="cta">
                                                <a class="btn <?php echo $style; ?>" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                                            </div>

                                        <?php endif; ?>

                                    <?php endforeach ?>
                                </div>
                            </div>
                        </div>

                        <div class="photo">
                            <div class="content">
                                <?php echo wp_get_attachment_image($photo['ID'], 'full'); ?>
                            </div>
                        </div>

                        <div class="caption">
                            <p><a href="<?php echo $page_link; ?>"><?php echo $caption; ?></a></p>
                        </div>                    
                    </div>
                <?php endwhile; endif; ?>
            </div>

            <div class="slider-controls">
                <div class="slider-controls__wrapper">
                    <span class="paging"></span>
                    <a href="#" class="custom-next">→</a>
                </div>
            </div>
        </div>
    </section>

<?php endwhile; endif; ?>