<?php

    $staging = get_field('staging');
    $headline = $staging['headline'];
    $copy = $staging['copy'];
    $link = $staging['link'];
    $photo = $staging['photo'];

?>

<section class="staging grid">
    <div class="counter">
        <div class="counter-wrapper">
            <h5>03</h5>
        </div>
    </div>

    <div class="headline">
        <h2 class="section-header"><?php echo $headline; ?></h2>
    </div>

    <?php if( $photo ): ?>
        <div class="photo">
            <?php echo wp_get_attachment_image($photo['ID'], 'full'); ?>
        </div>
    <?php endif; ?>

    <div class="copy-2">
        <?php echo $copy; ?>
    </div>

    <?php 
        if( $link ): 
        $link_url = $link['url'];
        $link_title = $link['title'];
        $link_target = $link['target'] ? $link['target'] : '_self';
    ?>

        <div class="cta">
            <a class="underline" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
        </div>

    <?php endif; ?>
</section>