<?php

/*
  
    Template Name: Home

*/

get_header(); ?>

    <?php get_template_part('templates/home/hero'); ?>

    <?php get_template_part('templates/home/about'); ?>

    <?php get_template_part('templates/home/hardwoods'); ?>

    <?php get_template_part('templates/home/finishes'); ?>

    <?php get_template_part('templates/home/staging'); ?>

    <?php get_template_part('template-parts/footer/about-contact'); ?>

<?php get_footer(); ?>