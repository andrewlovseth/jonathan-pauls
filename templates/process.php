<?php

/*
  
    Template Name: Process

*/

get_header(); ?>

    <?php get_template_part('template-parts/global/hero'); ?>

    <?php get_template_part('templates/process/overview'); ?>

    <?php get_template_part('templates/process/design'); ?>

    <?php get_template_part('templates/process/construction'); ?>

    <?php get_template_part('templates/process/finishing'); ?>

<?php get_footer(); ?>