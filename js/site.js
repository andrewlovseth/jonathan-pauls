(function ($, window, document, undefined) {

	$(document).ready(function($) {

		// rel="external"
		$('a[rel="external"]').click( function() {
			window.open( $(this).attr('href') );
			return false;
		});

		// Nav Trigger
		$('.js-nav-trigger').click(function(){
			$('body').toggleClass('nav-overlay-open');
			return false;
		});


		// Hero Slider
		var $slider = $('.hero-slider__wrapper');
		if ($slider.length) {
			var currentSlide;
			var slidesCount;
			var sliderCounter = $('.slider-controls .paging')

			var updateSliderCounter = function(slick, currentIndex) {
				currentSlide = slick.slickCurrentSlide() + 1;
				slidesCount = slick.slideCount;
				$(sliderCounter).html('0' + currentSlide + ' / 0' + slidesCount);
			};

			$slider.on('init', function(event, slick) {
				updateSliderCounter(slick);
			});

			$slider.on('afterChange', function(event, slick, currentSlide) {
				updateSliderCounter(slick, currentSlide);
			});

			$slider.slick({
				autoplay: true,
				autoplaySpeed: 8000,
				dots: false,
				arrows: true,
				infinite: true,
				speed: 750,
				slidesToShow: 1,
				nextArrow: '.custom-next'
			});
		}


		// FAQs
		$('.faq .question').on('click', function(){
			let faq = $(this).closest('.faq');
			faq.toggleClass('active');


			return false;
		});

		// History Slider
		$('.timeline-slider').slick({
			dots: false,
			arrows: false,
			infinite: true,
			speed: 300,
			slidesToShow: 1,
			centerMode: true,
			variableWidth: true
		});

		$('.timeline-slider').on('click', '.slick-slide', function (e) {
			e.stopPropagation();
			var index = $(this).data("slick-index");
			if ($('.timeline-slider').slick('slickCurrentSlide') !== index) {
				$('.timeline-slider').slick('slickGoTo', index);
			}
		});

		// Construction Slider
		$('.construction-slider').slick({
			dots: false,
			arrows: false,
			infinite: true,
			speed: 300,
			slidesToShow: 1,
			centerMode: true,
			variableWidth: true
		});
		  
		$('.construction-slider').on('click', '.slick-slide', function (e) {
			e.stopPropagation();
			var index = $(this).data("slick-index");
			if ($('.construction-slider').slick('slickCurrentSlide') !== index) {
				$('.construction-slider').slick('slickGoTo', index);
			}
		});


		// Process Finishing
		$('.feature .toggle').on('click', function(){
			let faq = $(this).closest('.feature');
			faq.toggleClass('active');


			return false;
		});


		// Case Study Slider
		$('.case-study-slider').slick({
			dots: true,
			arrows: false,
			infinite: true,
			speed: 300,
			slidesToShow: 1,
		});


	});

	$(document).mouseup(function(e) {
		var menu = $('.site-navigation, .js-nav-trigger');
	
		if (!menu.is(e.target) && menu.has(e.target).length === 0) {
			$('body').removeClass('nav-overlay-open');
		}
	});
	
	var macy = Macy({
		container: '.gallery .photos',
		margin: 16,
		columns: 4,
		breakAt: {
			992: 4,
			768: 3,
			320: 2
		}
	});

})(jQuery, window, document);