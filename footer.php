	</main> <!-- .site-content -->

	<footer class="site-footer grid">
		<?php get_template_part('template-parts/header/site-logo'); ?>
		
		<?php get_template_part('template-parts/footer/footer-navigation'); ?>

		<?php get_template_part('template-parts/footer/contact-info'); ?>

		<?php get_template_part('template-parts/footer/copyright'); ?>
	</footer>

<?php wp_footer(); ?>

</div> <!-- .site -->

</body>
</html>