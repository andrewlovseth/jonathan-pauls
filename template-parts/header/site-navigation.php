<?php if(have_rows('navigation', 'options')): ?>
    <nav class="site-navigation">
        <ul>
            <li class="mobile-only"><a href="<?php echo site_url('/'); ?>">Home</a></li>

            <?php while(have_rows('navigation', 'options')): the_row(); ?>
            
                <?php 
                    $link = get_sub_field('link');
                    if( $link ): 
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_slug = sanitize_title_with_dashes($link['title']);
                    $link_target = $link['target'] ? $link['target'] : '_self';
                ?>

                    <li>
                        <a href="<?php echo esc_url($link_url); ?>" class="nav-<?php echo $link_slug; ?>" target="<?php echo esc_attr($link_target); ?>">
                            <?php echo esc_html($link_title); ?>
                        </a>
                    </li>

                <?php endif; ?>

            <?php endwhile; ?>

            <?php get_template_part('template-parts/global/social-links'); ?>

        </ul>
    </nav>
<?php endif; ?>