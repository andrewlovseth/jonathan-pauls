<?php

    $hero = get_field('hero');
    $headline = $hero['headline'];
    $sub_headline = $hero['sub_headline'];
    $photo = $hero['photo'];

?>

<section class="hero default-hero grid">
    <div class="photo">
        <div class="content">
            <?php echo wp_get_attachment_image($photo['ID'], 'full'); ?>
        </div>
    </div>

    <div class="info">
        <div class="sub-headline">
            <h2><?php echo $sub_headline; ?></h2>
        </div>

        <div class="headline">
            <h1><?php echo $headline; ?></h1>
        </div>
    </div>
</section>