<?php if(have_rows('social', 'options')): ?>
    <?php while(have_rows('social', 'options')): the_row(); ?>

        <?php 
            $link = get_sub_field('link');
            if( $link ): 
            $link_url = $link['url'];
            $link_title = $link['title'];
            $link_slug = sanitize_title_with_dashes($link_title);
            $link_target = $link['target'] ? $link['target'] : '_self';
        ?>

            <li>
                <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
                    <?php get_template_part('template-parts/svg/' . $link_slug); ?>
                </a>
            </li>

        <?php endif; ?>

    <?php endwhile; ?>
<?php endif; ?>