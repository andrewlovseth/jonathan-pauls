<div class="contact-1 col">
    <div class="address detail">
        <div class="section-header">
            <h4>Address</h4>
        </div>

        <div class="info">
            <p><?php echo get_field('address', 'options'); ?></p>
        </div>
    </div>

    <div class="phone detail">
        <div class="section-header">
            <h4>Phone</h4>
        </div>

        <div class="info">
            <p><?php echo get_field('phone', 'options'); ?></p>
        </div>
    </div>

    <div class="email detail">
        <div class="section-header">
            <h4>Email</h4>
        </div>

        <div class="info">
            <p><a href="mailto:<?php echo get_field('email', 'options'); ?>"><?php echo get_field('email', 'options'); ?></a></p>
        </div>
    </div>
</div>

<div class="contact-2 col">
    <div class="hours detail">
        <div class="section-header">
            <h4>Hours</h4>
        </div>

        <div class="info">
            <p><?php echo get_field('hours', 'options'); ?></p>
        </div>
    </div>

    <div class="social detail">
        <div class="section-header">
            <h4>Social</h4>
        </div>
        
        <ul class="social-links">
            <?php get_template_part('template-parts/global/social-links'); ?>
        </ul>
    </div>
</div>