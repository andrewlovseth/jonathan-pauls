<?php

    $frontpage = get_option('page_on_front');

    $about_contact = get_field('about_contact', $frontpage);
    $headline = $about_contact['headline'];
    $copy = $about_contact['copy'];
    $link = $about_contact['link'];
    $contact_cta = $about_contact['contact_cta'];

?>

<section class="about-contact grid">
    <div class="history">
        <div class="headline">
            <h3><?php echo $headline; ?></h3>
        </div>

        <div class="details">
            <div class="copy-2">
                <?php echo $copy; ?>
            </div>

            <?php 
                if( $link ): 
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
            ?>

                <div class="cta">
                    <a class="underline" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                </div>

            <?php endif; ?>
        </div>
    </div>

    <div class="contact-cta">

        <?php 
            if( $contact_cta ): 
            $contact_url = $contact_cta['url'];
            $contact_title = $contact_cta['title'];
            $contact_target = $contact_cta['target'] ? $contact_cta['target'] : '_self';
        ?>

            <div class="cta">
                <a class="btn outline__white" href="<?php echo esc_url($contact_url); ?>" target="<?php echo esc_attr($contact_target); ?>"><?php echo esc_html($contact_title); ?></a>
            </div>

        <?php endif; ?>

    </div>
</section>