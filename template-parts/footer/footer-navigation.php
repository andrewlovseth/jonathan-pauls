<?php if(have_rows('footer_navigation', 'options')): ?>
    <nav class="footer-navigation col">
        <div class="section-header">
            <h4>Navigation</h4>
        </div>

        <ul>
            <?php while(have_rows('footer_navigation', 'options')): the_row(); ?>
            
                <?php 
                    $link = get_sub_field('link');
                    $child_page = get_sub_field('child_page');
                    if( $link ): 
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                ?>

                    <li<?php if($child_page): ?> class="child-page"<?php endif; ?>>
                        <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                    </li>

                <?php endif; ?>

            <?php endwhile; ?>
        </ul>
    </nav>
<?php endif; ?>